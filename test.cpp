#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>

#include <iostream>
#include <string>
#include <set>

struct rect
{
	int x;
	int y;
	int w;
	int h;
	std::set<int> nums;

	rect(int ax, int ay, int aw, int ah, std::set<int>& anums) : x(ax), y(ay), w(aw), h(ah), nums(anums) {}
};

template <typename T>
void check_intersections(const T& rects1, const T& rects2)
{
	std::vector<rect> rects_new;

	for (size_t cur = 0; cur < rects1.size(); ++cur) {
		for (size_t count = 0; count < rects2.size(); ++count) {

			if (rects1[cur].nums.find(count + 1) != rects1[cur].nums.end() ||
				rects1[cur].nums.upper_bound(count + 1) != rects1[cur].nums.end()) continue;

			int res_x, res_y, res_w, res_h;

			int r1_x = rects1[cur].x;
			int r1_y = rects1[cur].y;
			int r1_w = rects1[cur].w;
			int r1_h = rects1[cur].h;

			int r2_x = rects2[count].x;
			int r2_y = rects2[count].y;
			int r2_w = rects2[count].w;
			int r2_h = rects2[count].h;

			bool x_inter = false;
			bool y_inter = false;

			// checking
			if (r1_x <= r2_x and r1_x + r1_w >= r2_x) {
				x_inter = true;

				res_x = r2_x;
				res_w = std::min(r2_x + r2_w, r1_x + r1_w) - res_x;
			}
			if (r1_y <= r2_y and r1_y + r1_h >= r2_y) {
				y_inter = true;

				res_y = r2_y;
				res_h = std::min(r2_y + r2_h, r1_y + r1_h) - res_y;
			}
			if (r2_x <= r1_x and r2_x + r2_w >= r1_x) {
				x_inter = true;

				res_x = r1_x;
				res_w = std::min(r2_x + r2_w, r1_x + r1_w) - res_x;
			}
			if (r2_y <= r1_y and r2_y + r2_h >= r1_y) {
				y_inter = true;

				res_y = r1_y;
				res_h = std::min(r2_y + r2_h, r1_y + r1_h) - res_y;
			}

			if (x_inter && y_inter) {
				std::cout << "\tBetween rectangle ";

				bool first = true;
				for (auto val : rects1[cur].nums) { if (!first) std::cout << ", "; first = false; std::cout << val;}

				std::cout << " and " << (count + 1) << " at "
					<< "(" << res_x << "," << res_y << "), "
					<< "w=" << res_w << ", "
					<< "h=" << res_h << "." << std::endl;

				std::set<int> nums;
				for (auto val : rects1[cur].nums) nums.insert(val);
				nums.insert(count + 1);

				rects_new.push_back(rect(res_x, res_y, res_w, res_h, nums));
			}
		}
	}

	if (rects_new.size() > 0) check_intersections(rects_new, rects2);	
}

int main(int argc, char* argv[])
{
	if (argc != 2) {
		std::cout << "Using: test <file.json>" << std::endl;
		return 0;
	}

	namespace jp = boost::property_tree;

	jp::ptree root;
	jp::read_json(argv[1], root);

	size_t count = 0;
	std::vector<rect> rects;

	std::cout << "Input:" << std::endl;

	BOOST_FOREACH(auto& val, root.get_child("rects"))
	{
		auto& ch = val.second.get_child("");

		std::cout << "\t" << ++count << ": Rectangle at "
			<< "(" << ch.get<int>("x") << "," << ch.get<int>("y") << "), "
			<< "w=" << ch.get<int>("w") << ", "
			<< "h=" << ch.get<int>("h") << "."
			<< std::endl;

		std::set<int> nums;
		nums.insert(count);

		rects.push_back(
			rect(
				ch.get<int>("x"),
				ch.get<int>("y"),
				ch.get<int>("w"),
				ch.get<int>("h"),
				nums
			)
		);
	}

	std::cout << std::endl << "Intersections:" << std::endl;
	check_intersections(rects, rects);	

    return 0;
}
